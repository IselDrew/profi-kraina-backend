import isBase64 = require('is-base64');
import { cd } from '../common/config/cloudinary.config';
import { NotFound } from '../common/exceptions';

export async function deleteImage(publicId: string) {
  try {
    const del = await cd.uploader.destroy(publicId, (err, res) => {
      return err ? err : res;
    });
    return del;
  } catch (e) {
    throw new NotFound('Error updating avatar. Try again later');
  }
}

export async function uploadImage(base64Img: string) {
  if (!isBase64(base64Img, { mimeRequired: true })) {
    throw new Error('Unsupported file type');
  }
  try {
    const image = await cd.uploader.upload(base64Img, (err, res) => {
      return err ? err : res;
    });
    return image;
  } catch (e) {
    throw new Error('Image size is too large');
  }
}
