import express = require('express');
import cors = require('cors');
import * as bodyParser from 'body-parser';

import { createConnection } from 'typeorm';
const connectionOptions = require('./common/config/typeorm.config');
import * as dotenv from 'dotenv';
dotenv.config();

import { router as productRoutes } from './modules/product/product.routes';
import { router as eventRoutes } from './modules/event/event.routes';
import { router as adminRoutes } from './modules/admin/admin.routes';
import { router as clientRoutes } from './modules/client/client.routes';

import { productsAndEventsController } from './modules/productsAndEvents/productsAndEvents.controller';

import { errorHandler } from './common/middlewares/error.middleware';

class Server {
  private initDatabase(options: any) {
    return createConnection(options);
  }

  private initExpress() {
    const app: express.Express = express();
    app.use(bodyParser.json({ limit: '5mb' }));
    app.use(cors());

    app.get('/health', (req, res) => {
      res.send({ status: 'OK' });
    });

    app.use('/products', productRoutes);
    app.use('/events', eventRoutes);
    app.get('/products-events', productsAndEventsController.get);
    app.use('/clients', clientRoutes);

    app.use('/admin', adminRoutes);

    app.use(errorHandler);

    const PORT: number = process.env.PORT ? parseInt(process.env.PORT, 10) : 5000;

    return new Promise((resolve) => {
      app.listen(PORT, () => {
        console.log(`Server is running in http://localhost:${PORT}`);
        resolve();
      });
    });
  }

  async start() {
    await this.initDatabase(connectionOptions);
    console.log('Database connection has been established');

    await this.initExpress();
  }
}

const server = new Server();
server.start().catch((err) => {
  console.log(err);
});
