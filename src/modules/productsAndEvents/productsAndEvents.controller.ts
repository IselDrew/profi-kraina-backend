import { Request, Response, NextFunction } from 'express';

import { Brand } from '../brand/brand.entity';
import { ProductType } from '../productType/productType.entity';

import { brandService } from '../brand/brand.service';
import { productTypeService } from '../productType/productType.service';

class ProductsAndEventsController {
  public async get(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const brands: Brand[] = await brandService.findMany();
      const productTypes: ProductType[] = await productTypeService.findMany();
      res.json({
        brands,
        productTypes,
      });
    } catch (e) {
      next(e);
    }
  }
}

export const productsAndEventsController = new ProductsAndEventsController();
