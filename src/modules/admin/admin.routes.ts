import { Router } from 'express';

// import { eventTypeController } from '../eventType/eventType.controller';
import { eventController } from '../event/event.controller';
import { brandController } from '../brand/brand.controller';
import { productTypeController } from '../productType/productType.controller';
import { productController } from '../product/product.controller';

import { authMiddleware } from '../../common/middlewares/auth.middleware';

export const router: Router = Router();

router.post('/auth', authMiddleware);

// -------------------------- BRANDS -------------------------------
router.get('/brands', brandController.findMany);
router.get('/brands/:id', brandController.findOneById);

router.post('/brands', brandController.createOne);

router.put('/brands/:id', brandController.updateOne);

router.delete('/brands/:id', brandController.deleteOne);

// ----------------------- PRODUCT TYPES ---------------------------
router.get('/product-types/', productTypeController.findMany);
router.get('/product-types/:id', productTypeController.findOneById);

router.post('/product-types/', productTypeController.createOne);

router.put('/product-types/:id', productTypeController.updateOne);

router.delete('/product-types/:id', productTypeController.deleteOne);

// --------------------------- PRODUCTS ----------------------------
router.get('/products', productController.findMany);
router.get('/products/:id', productController.findOneById);

router.post('/products/', productController.createOne);

router.put('/products/:id', productController.updateOne);

router.delete('/products/:id', productController.deleteOne);

// ------------------------ EVENT TYPES ----------------------------
// router.get('/eventTypes', eventTypeController.findMany);
// router.get('/eventTypes/:id', eventTypeController.findOneById);

// router.post('/eventTypes/', eventTypeController.createOne);

// router.put('/eventTypes/:id', eventTypeController.updateOne);

// router.delete('/eventTypes/:id', eventTypeController.deleteOne);

// --------------------------- EVENTS ------------------------------
router.get('/events', eventController.findMany);
router.get('/events/:id', eventController.findOneById);

router.post('/events', eventController.createOne);

router.put('/events/:id', eventController.updateOne);

router.delete('/events/:id', eventController.deleteOne);
