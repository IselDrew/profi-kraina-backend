import { Router } from 'express';
import { eventController } from './event.controller';

export const router: Router = Router();

router.get('/', eventController.findByEventType);

router.get('/:id', eventController.findOneById);
