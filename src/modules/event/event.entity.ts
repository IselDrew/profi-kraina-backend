import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToOne,
  JoinColumn,
} from 'typeorm';

import { Image } from '../image/image.entity';

@Entity({ name: 'events' })
export class Event {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ type: 'varchar', length: 225 })
  name!: string;

  @Column({ type: 'text' })
  description?: string;

  @Column({ type: 'varchar', length: 225 })
  location?: string;

  @Column({ type: 'timestamp with time zone' })
  date?: Date;

  @Column({ type: 'float' })
  price?: number;

  @Column({ type: 'integer' })
  imageId!: number;

  @CreateDateColumn({ type: 'timestamp with time zone', default: () => 'NOW()' })
  createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp with time zone', default: () => 'NOW()' })
  updatedAt!: Date;

  @OneToOne(() => Image, (image) => image.event)
  @JoinColumn()
  image!: Image;
}
