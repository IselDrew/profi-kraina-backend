import { getManager, getRepository, Raw } from 'typeorm';
import { NotFound } from '../../common/exceptions';

import { Event } from './event.entity';
import { Image } from '../image/image.entity';
import { IOffset, IPaginatedData } from '../../common/interfaces/pagination.interfaces';

import { paginationService } from '../pagination/pagination.service';
import { uploadImage, deleteImage } from '../../utils/cloudinary.uploader';

class EventService {
  public async findByEventType(query: qs.ParsedQs): Promise<IPaginatedData<Event[]>> {
    const page = Number(query.page);
    const limit = 3;
    const searchedItem = query.search || '';

    const searchOptions = {
      name: Raw((alias) => `LOWER(${alias}) ILIKE '%${searchedItem}%'`), // check if LOWER is needed
    };

    return getManager().transaction(async (transactionalEntityManager) => {
      const total: number = await transactionalEntityManager
        .getRepository(Event)
        .count({ where: searchOptions });
      const { offset, currentPage }: IOffset = paginationService.getOffset(page, limit, total);

      const events: Event[] = await getRepository(Event).find({
        relations: ['image'],
        order: { id: 'ASC' },
        skip: offset,
        take: limit,
        where: searchOptions,
      });

      return {
        data: events,
        pagination: {
          page: currentPage,
          total,
          limit,
        },
      };
    });
  }

  public async findOneById(id: number): Promise<Event> {
    const event: Event | undefined = await getRepository(Event).findOne({
      where: { id },
      relations: ['image'],
    });
    if (!event) {
      throw new NotFound(`There is no event with id ${id}`);
    }
    return event;
  }

  public async findMany(): Promise<Event[]> {
    return getRepository(Event).find({ relations: ['image'] });
  }

  public async createOne(newEvent: any): Promise<Event> {
    const {
      name,
      description,
      location,
      date,
      price,
      image: { src },
    }: any = newEvent;
    return getManager().transaction(async (transactionalEntityManager) => {
      // uploading image to cloudinary
      const { url, public_id: publicId } = await uploadImage(src);
      const image = {
        url,
        publicId,
      };

      // adding image to db
      const uploadedImage: any = await transactionalEntityManager
        .getRepository(Image)
        .create(image);
      await transactionalEntityManager.getRepository(Image).save(uploadedImage);

      // forming event object
      const event = {
        name,
        description,
        location,
        date,
        price: Number(price),
        imageId: uploadedImage.id,
      };

      // adding event to db
      const createdEvent: any = await transactionalEntityManager.getRepository(Event).create(event);
      await transactionalEntityManager.getRepository(Event).save(createdEvent);
      return createdEvent;
    });
  }

  public async updateOne(id: number, updates: Event): Promise<Event> {
    const {
      name,
      description,
      location,
      date,
      price,
      image: { src },
    }: any = updates;
    return getManager().transaction(async (transactionalEntityManager) => {
      // check if event to update exists
      const existedEvent: Event | undefined = await transactionalEntityManager
        .getRepository(Event)
        .findOne({ where: { id }, relations: ['image'] });
      if (!existedEvent) {
        throw new NotFound(`There is no event with id ${id}`);
      }

      // remove image from cloud and db
      const {
        image: { publicId: removeId },
      } = existedEvent;

      await deleteImage(removeId);

      // upload new image
      const { url, public_id: publicId } = await uploadImage(src);
      const image = {
        url,
        publicId,
      };

      const uploadedImage: any = await transactionalEntityManager
        .getRepository(Image)
        .create(image);
      await transactionalEntityManager.getRepository(Image).save(uploadedImage);

      // BUG: there is no remove image from db

      // forming event object
      const updatedEvent = {
        name,
        description,
        location,
        date,
        price: Number(price),
        imageId: uploadedImage.id,
      };

      await transactionalEntityManager.getRepository(Event).update(id, updatedEvent);
      const event: Event = await transactionalEntityManager.getRepository(Event).findOneOrFail(id);
      return event;
    });
  }

  public async deleteOne(id: number): Promise<number> {
    return getManager().transaction(async (transactionalEntityManager) => {
      const event: Event | undefined = await transactionalEntityManager
        .getRepository(Event)
        .findOne({ where: { id }, relations: ['image'] });
      if (!event) {
        throw new NotFound(`There is no event with id ${id}`);
      }

      const {
        image: { id: imageId, publicId },
      } = event;

      const existedImage: Image | undefined = await transactionalEntityManager
        .getRepository(Image)
        .findOne(imageId);
      if (!existedImage) {
        throw new NotFound(`There is no product with id ${id}`);
      }

      await transactionalEntityManager.getRepository(Event).remove(event);
      await transactionalEntityManager.getRepository(Image).remove(existedImage);
      await deleteImage(publicId);

      return id;
    });
  }
}

export const eventService = new EventService();
