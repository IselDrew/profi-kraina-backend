import { Request, Response, NextFunction } from 'express';

import { IPaginatedData } from '../../common/interfaces/pagination.interfaces';

import { eventService } from './event.service';
import { Event } from './event.entity';

class EventController {
  public async findByEventType(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const events: IPaginatedData<Event[]> = await eventService.findByEventType(req.query);
      res.json(events);
    } catch (e) {
      next(e);
    }
  }

  public async findOneById(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const id = parseInt(req.params.id, 10);
      const event: Event = await eventService.findOneById(id);
      res.json(event);
    } catch (e) {
      next(e);
    }
  }

  // ----------------------------------- ADMIN METHODS -----------------------------------
  public async findMany(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const events: Event[] = await eventService.findMany();

      res.setHeader('Content-Range', `events 0-20/${events.length}`);
      res.setHeader('Access-Control-Expose-Headers', 'Content-Range');
      res.json(events);
    } catch (e) {
      next(e);
    }
  }

  public async createOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const createdEvent: Event = await eventService.createOne(req.body);
      res.json(createdEvent);
    } catch (e) {
      next(e);
    }
  }

  public async updateOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const id = parseInt(req.params.id, 10);
      const updatedEvent: Event = await eventService.updateOne(id, req.body);
      res.json(updatedEvent);
    } catch (e) {
      next(e);
    }
  }

  public async deleteOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const id = parseInt(req.params.id, 10);
      const deletedEventId: number = await eventService.deleteOne(id);
      res.json({ id: deletedEventId });
    } catch (e) {
      next(e);
    }
  }
}

export const eventController = new EventController();
