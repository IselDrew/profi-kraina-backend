// import { Request, Response, NextFunction } from 'express';

// import { eventTypeService } from './eventType.service';
// import { EventType } from './eventType.entity';

// class EventTypeController {
//   public async findMany(req: Request, res: Response, next: NextFunction): Promise<void> {
//     try {
//       const eventTypes: EventType[] = await eventTypeService.findMany();
//       res.json(eventTypes);
//     } catch (e) {
//       next(e);
//     }
//   }

//   public async findOneById(req: Request, res: Response, next: NextFunction): Promise<void> {
//     try {
//       const id = parseInt(req.params.id, 10);
//       const eventType: EventType = await eventTypeService.findOneById(id);
//       res.json(eventType);
//     } catch (e) {
//       next(e);
//     }
//   }

//   public async createOne(req: Request, res: Response, next: NextFunction): Promise<void> {
//     try {
//       const createdEventType: EventType = await eventTypeService.createOne(req.body);
//       res.json(createdEventType);
//     } catch (e) {
//       next(e);
//     }
//   }

//   public async updateOne(req: Request, res: Response, next: NextFunction): Promise<void> {
//     try {
//       const id = parseInt(req.params.id, 10);
//       const updatedEventType: EventType = await eventTypeService.updateOne(id, req.body);
//       res.json(updatedEventType);
//     } catch (e) {
//       next(e);
//     }
//   }

//   public async deleteOne(req: Request, res: Response, next: NextFunction): Promise<void> {
//     try {
//       const id = parseInt(req.params.id, 10);
//       const deletedEventTypeId: number = await eventTypeService.deleteOne(id);
//       res.json({ id: deletedEventTypeId });
//     } catch (e) {
//       next(e);
//     }
//   }
// }

// export const eventTypeController = new EventTypeController();
