// import { getManager, getRepository } from 'typeorm';
// import { NotFound } from '../../common/exceptions';

// import { EventType } from './eventType.entity';

// class EventTypeService {
//   public async findMany(): Promise<EventType[]> {
//     return getRepository(EventType).find();
//   }

//   public async findOneById(id: number): Promise<EventType> {
//     const eventType: EventType | undefined = await getRepository(EventType).findOne(id);
//     if (!eventType) {
//       throw new NotFound(`There is no eventType with id ${id}`);
//     }
//     return eventType;
//   }

//   public async createOne(newEventType: EventType): Promise<EventType> {
//     const createdEventType: EventType = await getRepository(EventType).create(newEventType);
//     await getRepository(EventType).save(createdEventType);
//     return createdEventType;
//   }

//   public async updateOne(id: number, updates: EventType): Promise<EventType> {
//     return getManager().transaction(async (transactionalEntityManager) => {
//       const eventType: EventType | undefined = await transactionalEntityManager
//         .getRepository(EventType)
//         .findOne(id);
//       if (!eventType) {
//         throw new NotFound(`There is no eventType with id ${id}`);
//       }

//       await transactionalEntityManager.getRepository(EventType).update(id, updates);

//       const updatedEventType: EventType = await transactionalEntityManager
//         .getRepository(EventType)
//         .findOneOrFail(id);
//       return updatedEventType;
//     });
//   }

//   public async deleteOne(id: number): Promise<number> {
//     return getManager().transaction(async (transactionalEntityManager) => {
//       const eventType: EventType | undefined = await transactionalEntityManager
//         .getRepository(EventType)
//         .findOne(id);
//       if (!eventType) {
//         throw new NotFound(`There is no eventType with id ${id}`);
//       }

//       await transactionalEntityManager.getRepository(EventType).remove(eventType);
//       return id;
//     });
//   }
// }

// export const eventTypeService = new EventTypeService();
