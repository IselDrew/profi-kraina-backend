import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne,
} from 'typeorm';

import { Event } from '../event/event.entity';
import { Product } from '../product/product.entity';

@Entity({ name: 'images' })
export class Image {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ type: 'varchar', length: 225 })
  url?: string;

  @Column({ type: 'varchar', length: 225 })
  publicId!: string;

  @CreateDateColumn({ type: 'timestamp with time zone', default: () => 'NOW()' })
  createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp with time zone', default: () => 'NOW()' })
  updatedAt!: Date;

  @OneToOne(() => Event, (event) => event.image)
  event!: Event;

  @OneToOne(() => Product, (product) => product.image)
  product!: Product;
}
