import { getManager, getRepository } from 'typeorm';
import { NotFound } from '../../common/exceptions';
import { NodeMailer } from '../../common/nodeMailer/nodeMailer';

import { Client } from './client.entity';
import { IClientData } from '../../common/interfaces/client.interface';

class ClientService {
  public async findMany(): Promise<Client[]> {
    return getRepository(Client).find({ relations: ['events'] });
  }

  public async findOneById(id: number): Promise<Client> {
    const client: Client | undefined = await getRepository(Client).findOne(id);
    if (!client) {
      throw new NotFound(`There is no client with id ${id}`);
    }
    return client;
  }

  public async createOne(newClient: IClientData): Promise<Client | undefined> {
    const { city, eventId, email } = newClient;
    if (!city && !eventId) {
      await NodeMailer.fireMessage(newClient);
      return;
    }

    return getManager().transaction(async (transactionalEntityManager) => {
      await NodeMailer.fireMessage(newClient);
      delete newClient.eventId;

      const client: Client | undefined = await transactionalEntityManager
        .getRepository(Client)
        .findOne({
          email,
        });
      if (client) {
        return client;
      }

      const createdClient: Client = await transactionalEntityManager
        .getRepository(Client)
        .create(newClient);
      await transactionalEntityManager.getRepository(Client).save(createdClient);

      return createdClient;
    });
  }

  public async updateOne(id: number, updates: Client): Promise<Client> {
    return getManager().transaction(async (transactionalEntityManager) => {
      const client: Client | undefined = await transactionalEntityManager
        .getRepository(Client)
        .findOne(id);
      if (!client) {
        throw new NotFound(`There is no client with id ${id}`);
      }

      await transactionalEntityManager.getRepository(Client).update(id, updates);

      const updatedClient: Client = await transactionalEntityManager
        .getRepository(Client)
        .findOneOrFail(id);
      return updatedClient;
    });
  }

  public async deleteOne(id: number): Promise<number> {
    return getManager().transaction(async (transactionalEntityManager) => {
      const client: Client | undefined = await transactionalEntityManager
        .getRepository(Client)
        .findOne(id);
      if (!client) {
        throw new NotFound(`There is no client with id ${id}`);
      }

      await transactionalEntityManager.getRepository(Client).remove(client);
      return id;
    });
  }
}

export const clientService = new ClientService();
