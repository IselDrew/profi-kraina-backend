import { Request, Response, NextFunction } from 'express';

import { productTypeService } from './productType.service';
import { ProductType } from './productType.entity';

class ProductTypeController {
  public async findMany(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const productTypes: ProductType[] = await productTypeService.findMany();

      res.setHeader('Content-Range', `product-types 0-20/${productTypes.length}`);
      res.setHeader('Access-Control-Expose-Headers', 'Content-Range');
      res.json(productTypes);
    } catch (e) {
      next(e);
    }
  }

  public async findOneById(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const id = parseInt(req.params.id, 10);
      const productType: ProductType = await productTypeService.findOneById(id);
      res.json(productType);
    } catch (e) {
      next(e);
    }
  }

  public async createOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const createdProductType: ProductType = await productTypeService.createOne(req.body);
      res.json(createdProductType);
    } catch (e) {
      next(e);
    }
  }

  public async updateOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const id = parseInt(req.params.id, 10);
      const updatedProductType: ProductType = await productTypeService.updateOne(id, req.body);
      res.json(updatedProductType);
    } catch (e) {
      next(e);
    }
  }

  public async deleteOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const id = parseInt(req.params.id, 10);
      const deletedProductTypeId: number = await productTypeService.deleteOne(id);
      res.json({ id: deletedProductTypeId });
    } catch (e) {
      next(e);
    }
  }
}

export const productTypeController = new ProductTypeController();
