import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  // ManyToOne,
} from 'typeorm';

import { Product } from '../product/product.entity';
// import { Brand } from '../brand/brand.entity';

@Entity({ name: 'productTypes' })
export class ProductType {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ type: 'varchar', length: 225 })
  name!: string;

  // @Column({ type: 'integer' })
  // brandId!: string;

  @CreateDateColumn({ type: 'timestamp with time zone', default: () => 'NOW()' })
  createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp with time zone', default: () => 'NOW()' })
  updatedAt!: Date;

  @OneToMany((type) => Product, (product) => product.productType)
  products?: Product[];

  // @ManyToOne((type) => Brand, (brand) => brand.productTypes)
  // brand?: Brand;
}
