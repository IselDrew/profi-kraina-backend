import { getManager, getRepository } from 'typeorm';
import { NotFound } from '../../common/exceptions';

import { ProductType } from './productType.entity';

class ProductTypeService {
  public async findMany(): Promise<ProductType[]> {
    return getRepository(ProductType).find({
      order: { id: 'ASC' },
    });
  }

  public async findOneById(id: number): Promise<ProductType> {
    const productType: ProductType | undefined = await getRepository(ProductType).findOne(id);
    if (!productType) {
      throw new NotFound(`There is no productType with id ${id}`);
    }
    return productType;
  }

  public async createOne(newProductType: ProductType): Promise<ProductType> {
    const createdProductType: ProductType = await getRepository(ProductType).create(newProductType);
    await getRepository(ProductType).save(createdProductType);
    return createdProductType;
  }

  public async updateOne(id: number, updates: ProductType): Promise<ProductType> {
    return getManager().transaction(async (transactionalEntityManager) => {
      const productType: ProductType | undefined = await transactionalEntityManager
        .getRepository(ProductType)
        .findOne(id);
      if (!productType) {
        throw new NotFound(`There is no productType with id ${id}`);
      }

      await transactionalEntityManager.getRepository(ProductType).update(id, updates);

      const updatedProductType: ProductType = await transactionalEntityManager
        .getRepository(ProductType)
        .findOneOrFail(id);
      return updatedProductType;
    });
  }

  public async deleteOne(id: number): Promise<number> {
    return getManager().transaction(async (transactionalEntityManager) => {
      const productType: ProductType | undefined = await transactionalEntityManager
        .getRepository(ProductType)
        .findOne(id);
      if (!productType) {
        throw new NotFound(`There is no productType with id ${id}`);
      }

      await transactionalEntityManager.getRepository(ProductType).remove(productType);
      return id;
    });
  }
}

export const productTypeService = new ProductTypeService();
