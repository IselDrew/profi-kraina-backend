import { getManager, getRepository } from 'typeorm';
import { NotFound } from '../../common/exceptions';

import { Brand } from './brand.entity';

class BrandService {
  public async findMany(): Promise<Brand[]> {
    return getRepository(Brand).find({
      order: { name: 'ASC' },
    });
  }

  public async findOneById(id: number): Promise<Brand> {
    const brand: Brand | undefined = await getRepository(Brand).findOne(id);
    if (!brand) {
      throw new NotFound(`There is no brand with id ${id}`);
    }
    return brand;
  }

  public async createOne(newBrand: Brand): Promise<Brand> {
    const createdBrand: Brand = await getRepository(Brand).create(newBrand);
    await getRepository(Brand).save(createdBrand);
    return createdBrand;
  }

  public async updateOne(id: number, updates: Brand): Promise<Brand> {
    return getManager().transaction(async (transactionalEntityManager) => {
      const brand: Brand | undefined = await transactionalEntityManager
        .getRepository(Brand)
        .findOne(id);
      if (!brand) {
        throw new NotFound(`There is no brand with id ${id}`);
      }

      await transactionalEntityManager.getRepository(Brand).update(id, updates);

      const updatedBrand: Brand = await transactionalEntityManager
        .getRepository(Brand)
        .findOneOrFail(id);
      return updatedBrand;
    });
  }

  public async deleteOne(id: number): Promise<number> {
    return getManager().transaction(async (transactionalEntityManager) => {
      const brand: Brand | undefined = await transactionalEntityManager
        .getRepository(Brand)
        .findOne(id);
      if (!brand) {
        throw new NotFound(`There is no brand with id ${id}`);
      }

      await transactionalEntityManager.getRepository(Brand).remove(brand);
      return id;
    });
  }
}

export const brandService = new BrandService();
