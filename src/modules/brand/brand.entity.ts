import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';

// import { ProductType } from '../productType/productType.entity';
import { Product } from '../product/product.entity';

@Entity({ name: 'brands' })
export class Brand {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ type: 'varchar', length: 225 })
  name!: string;

  @CreateDateColumn({ type: 'timestamp with time zone', default: () => 'NOW()' })
  createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp with time zone', default: () => 'NOW()' })
  updatedAt!: Date;

  // @OneToMany((type) => ProductType, (productType) => productType.brand)
  // productTypes?: ProductType[];

  @OneToMany((type) => Product, (product) => product.brand)
  products?: Product[];
}
