import { Request, Response, NextFunction } from 'express';

import { brandService } from './brand.service';
import { Brand } from './brand.entity';

class BrandController {
  public async findMany(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const brands: Brand[] = await brandService.findMany();

      res.setHeader('Content-Range', `brand 0-20/${brands.length}`);
      res.setHeader('Access-Control-Expose-Headers', 'Content-Range');
      res.json(brands);
    } catch (e) {
      next(e);
    }
  }

  public async findOneById(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const id = parseInt(req.params.id, 10);
      const brand: Brand = await brandService.findOneById(id);
      res.json(brand);
    } catch (e) {
      next(e);
    }
  }

  public async createOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const createdBrand: Brand = await brandService.createOne(req.body);
      res.json(createdBrand);
    } catch (e) {
      next(e);
    }
  }

  public async updateOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const id = parseInt(req.params.id, 10);
      const updatedBrand: Brand = await brandService.updateOne(id, req.body);
      res.json(updatedBrand);
    } catch (e) {
      next(e);
    }
  }

  public async deleteOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const id = parseInt(req.params.id, 10);
      const deletedBrandId: number = await brandService.deleteOne(id);
      res.json({ id: deletedBrandId });
    } catch (e) {
      next(e);
    }
  }
}

export const brandController = new BrandController();
