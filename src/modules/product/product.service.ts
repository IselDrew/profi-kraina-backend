import { getManager, getRepository, Raw } from 'typeorm';
import { NotFound } from '../../common/exceptions';
import { uploadImage, deleteImage } from '../../utils/cloudinary.uploader';

import { Image } from '../image/image.entity';
import { Product } from './product.entity';
import { IOffset, IPaginatedData } from '../../common/interfaces/pagination.interfaces';

import { paginationService } from '../pagination/pagination.service';
import { productTypeService } from '../productType/productType.service';

class ProductService {
  public async findByProductTypeOrBrand(query: qs.ParsedQs): Promise<IPaginatedData<Product[]>> {
    const productTypeId = Number(query.productTypeId) || null;
    const brandId = Number(query.brandId) || null;
    const page = Number(query.page) || 1;
    const limit = 9;
    const searchedItem = query.search || '';

    const searchOptions = {
      name: Raw((alias) => `LOWER(${alias}) ILIKE '%${searchedItem}%'`), // check if LOWER is needed
      ...(productTypeId && { productTypeId }),
      ...(brandId && { brandId }),
    };

    return getManager().transaction(async (transactionalEntityManager) => {
      const total: number = await transactionalEntityManager
        .getRepository(Product)
        .count({ where: searchOptions });
      const { offset, currentPage }: IOffset = paginationService.getOffset(page, limit, total);

      const products: Product[] = await getRepository(Product).find({
        relations: ['productType', 'image', 'brand'],
        order: { id: 'ASC' },
        skip: offset,
        take: limit,
        where: searchOptions,
      });

      return {
        data: products,
        pagination: {
          page: currentPage,
          total,
          limit,
        },
      };
    });
  }

  public async findMany(): Promise<Product[]> {
    return getRepository(Product).find({
      relations: ['productType', 'brand', 'image'],
    });
  }

  public async findOneById(id: number): Promise<Product> {
    const product: Product | undefined = await getRepository(Product).findOne({
      where: { id },
      relations: ['image', 'brand', 'productType'],
    });
    if (!product) {
      throw new NotFound(`There is no product with id ${id}`);
    }
    return product;
  }

  public async createOne(newProduct: any): Promise<Product> {
    const {
      productTypeId,
      brandId,
      name,
      details,
      image: { src },
    }: any = newProduct;

    return getManager().transaction(async (transactionalEntityManager) => {
      // throws error if there is no such type id
      await productTypeService.findOneById(productTypeId);

      const { url, public_id: publicId } = await uploadImage(src);
      const image = {
        url,
        publicId,
      };

      const uploadedImage: any = await transactionalEntityManager
        .getRepository(Image)
        .create(image);
      await transactionalEntityManager.getRepository(Image).save(uploadedImage);

      const product = {
        name,
        details,
        productTypeId,
        brandId,
        imageId: uploadedImage.id,
      };

      const createdProduct: any = await transactionalEntityManager
        .getRepository(Product)
        .create(product);

      await transactionalEntityManager.getRepository(Product).save(createdProduct);
      return createdProduct;
    });
  }

  public async updateOne(id: number, updates: any): Promise<Product> {
    const {
      brandId,
      details,
      image: { src },
      name,
      productTypeId,
    } = updates;

    return getManager().transaction(async (transactionalEntityManager) => {
      // check if product to update exists
      const existedProduct: Product | undefined = await transactionalEntityManager
        .getRepository(Product)
        .findOne({ where: { id }, relations: ['image'] });
      if (!existedProduct) {
        throw new NotFound(`There is no product with id ${id}`);
      }

      // remove image from cloud
      const {
        image: { publicId: removeId },
      } = existedProduct;
      await deleteImage(removeId);

      // upload new image
      const { url, public_id: publicId } = await uploadImage(src);
      const image = {
        url,
        publicId,
      };

      const uploadedImage: any = await transactionalEntityManager
        .getRepository(Image)
        .create(image);
      await transactionalEntityManager.getRepository(Image).save(uploadedImage);
      // BUG: there is no remove image from db

      // create object to update
      const updatedProduct = {
        name,
        details,
        productTypeId,
        brandId,
        imageId: uploadedImage.id,
      };

      // update product
      await transactionalEntityManager.getRepository(Product).update(id, updatedProduct);
      const product: Product = await transactionalEntityManager
        .getRepository(Product)
        .findOneOrFail(id);
      return product;
    });
  }

  public async deleteOne(id: number): Promise<number> {
    return getManager().transaction(async (transactionalEntityManager) => {
      const product: Product | undefined = await transactionalEntityManager
        .getRepository(Product)
        .findOne({ where: { id }, relations: ['image'] });
      if (!product) {
        throw new NotFound(`There is no product with id ${id}`);
      }
      const {
        image: { id: imageId, publicId },
      } = product;

      const existedImage: Image | undefined = await transactionalEntityManager
        .getRepository(Image)
        .findOne(imageId);
      if (!existedImage) {
        throw new NotFound(`There is no product with id ${id}`);
      }
      await transactionalEntityManager.getRepository(Product).remove(product);
      await transactionalEntityManager.getRepository(Image).remove(existedImage);
      await deleteImage(publicId);

      return id;
    });
  }
}

export const productService = new ProductService();
