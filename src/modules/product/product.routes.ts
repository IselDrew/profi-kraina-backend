import { Router } from 'express';
import { productController } from './product.controller';

export const router: Router = Router();

router.get('/', productController.findByProductTypeOrBrand);
