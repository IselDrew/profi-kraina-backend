import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToOne,
  JoinColumn,
} from 'typeorm';

import { ProductType } from '../productType/productType.entity';
import { Brand } from '../brand/brand.entity';
import { Image } from '../image/image.entity';

@Entity({ name: 'products' })
export class Product {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ type: 'varchar', length: 225 })
  name!: string;

  @Column({ type: 'text' })
  details!: string;

  @Column({ type: 'integer' })
  productTypeId?: number;

  @Column({ type: 'integer' })
  brandId?: number;

  @Column({ type: 'integer' })
  imageId!: number;

  @CreateDateColumn({ type: 'timestamp with time zone', default: () => 'NOW()' })
  createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp with time zone', default: () => 'NOW()' })
  updatedAt!: Date;

  @ManyToOne((type) => ProductType, (productType) => productType.products)
  productType!: ProductType;

  @ManyToOne((type) => Brand, (brand) => brand.products)
  brand!: Brand;

  @OneToOne(() => Image, (image) => image.product)
  @JoinColumn()
  image!: Image;
}
