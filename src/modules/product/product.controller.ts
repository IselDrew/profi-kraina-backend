import { Request, Response, NextFunction } from 'express';

import { Product } from './product.entity';
import { IPaginatedData } from '../../common/interfaces/pagination.interfaces';

import { productService } from './product.service';

class ProductController {
  public async findByProductTypeOrBrand(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      const products: IPaginatedData<Product[]> = await productService.findByProductTypeOrBrand(
        req.query
      );
      res.json(products);
    } catch (e) {
      next(e);
    }
  }

  public async findMany(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const products: Product[] = await productService.findMany();

      res.setHeader('Content-Range', `products 0-20/${products.length}`);
      res.setHeader('Access-Control-Expose-Headers', 'Content-Range');
      res.json(products);
    } catch (e) {
      next(e);
    }
  }

  public async findOneById(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const id = parseInt(req.params.id, 10);
      const product: Product = await productService.findOneById(id);
      res.json(product);
    } catch (e) {
      next(e);
    }
  }

  public async createOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const createdProduct: Product = await productService.createOne(req.body);
      res.json(createdProduct);
    } catch (e) {
      next(e);
    }
  }

  public async updateOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const id = parseInt(req.params.id, 10);
      const updatedProduct: Product = await productService.updateOne(id, req.body);
      res.json(updatedProduct);
    } catch (e) {
      next(e);
    }
  }

  public async deleteOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const id = parseInt(req.params.id, 10);
      const deletedProductId: number = await productService.deleteOne(id);
      res.json({ id: deletedProductId });
    } catch (e) {
      next(e);
    }
  }
}

export const productController = new ProductController();
