import { Client } from '../../modules/client/client.entity';

export interface IClientData extends Client {
  eventId?: number;
}
