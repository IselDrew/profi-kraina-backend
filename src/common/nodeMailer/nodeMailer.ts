import * as nodemailer from 'nodemailer';

import { IClientData } from '../interfaces/client.interface';
import { Event } from '../../modules/event/event.entity';

import { eventService } from '../../modules/event/event.service';

export class NodeMailer {
  static async fireMessage(clientData: IClientData) {
    const transporter = nodemailer.createTransport({
      service: 'gmail',
      port: 587,
      secure: false,
      requireTLS: true,
      auth: {
        user: process.env.GMAIL_ACCOUNT,
        pass: process.env.GMAIL_PASSWORD,
      },
    });

    const { firstName, lastName, phoneNumber, email, city, eventId } = clientData;
    const userDataHtml = `
      <div>Данные пользователя:<div>
      <br/>
      <div>Имя: ${firstName}</div>
      <div>Фамилия: ${lastName}</div>
      ${city ? `<div>Город: ${city}</div>` : ''}
      <div>Телефон: ${phoneNumber}</div>
      <div>Электронная почта: ${email}</div>
    `;

    const mailOptions: nodemailer.SendMailOptions = {
      from: 'Profi-kraina <andryshonochek0604@gmail.com>',
      to: process.env.GMAIL_ACCOUNT,
    };

    if (eventId) {
      const { name }: Event = await eventService.findOneById(eventId);

      mailOptions.subject = 'Регистрация на курс';
      mailOptions.html = `
        <div>Пользователь зарегистрировался на курс.<div>
        <br/>
        ${userDataHtml}
        <div>Курс: ${name}</div>
      `;
    } else {
      mailOptions.subject = 'Контактная форма';
      mailOptions.html = `
        <div>Пользователь оставил контактную форму.<div>
        <br/>
        ${userDataHtml}
      `;
    }

    transporter.sendMail(mailOptions, (err, info) => {
      err ? console.log(err) : console.log(info);
    });
  }
}
