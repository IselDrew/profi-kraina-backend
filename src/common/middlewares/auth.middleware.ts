import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';

export const authMiddleware = (req: Request, res: Response, next: NextFunction) => {
  const { username, password } = req.body;
  const secret = process.env.JWT_KEY || 'secret';
  if (username === process.env.ADMIN_USERNAME && password === process.env.ADMIN_PASSWORD) {
    const token = jwt.sign(
      {
        username,
      },
      secret,
      {
        expiresIn: '1h',
      }
    );
    return res.status(200).json({
      message: 'Auth succesful',
      token,
    });
  } else {
    return res.status(401).json({
      message: 'Auth failed',
    });
  }
};
