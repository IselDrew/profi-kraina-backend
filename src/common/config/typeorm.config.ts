import * as dotenv from 'dotenv';
import { DatabaseType } from 'typeorm';

// entities
import { Product } from '../../modules/product/product.entity';
import { ProductType } from '../../modules/productType/productType.entity';
import { Brand } from '../../modules/brand/brand.entity';
import { Event } from '../../modules/event/event.entity';
// import { EventType } from '../../modules/eventType/eventType.entity';
import { Client } from '../../modules/client/client.entity';
import { Image } from '../../modules/image/image.entity';

dotenv.config();

let config = {};
if (process.env.NODE_ENV === 'production') {
  console.log('Establishing production config');
  config = {
    type: 'postgres',
    url: process.env.DATABASE_URL,
    synchronize: false,
    logging: true,
    entities: [Product, ProductType, Brand, Event, Client, Image],
    migrations: ['/migration/**/*.ts'],
    extra: {
      ssl: true,
    },
    cli: {
      migrationsDir: '/migration/**/*.ts',
    },
  };
} else {
  config = {
    host: process.env.HOST,
    type: process.env.TYPE as DatabaseType,
    port: Number(process.env.DB_PORT),
    username: process.env.USER,
    password: process.env.PASSWORD,
    database: process.env.DATABASE,
    synchronize: false,
    entities: [Product, ProductType, Brand, Event, Client, Image],
    migrations: ['src/migration/**/*.ts'],
    cli: {
      migrationsDir: 'src/migration',
      entitiesDir: 'src/entity',
      subscribersDir: 'src/subscriber',
    },
  };
}

module.exports = config;
