console.log('process.env.DATABASE_URL:', process.env.DATABASE_URL);
module.exports = {
  type: 'postgres',
  url: process.env.DATABASE_URL,
  synchronize: false,
  migrations: ['build/migration/**/*.js'],
  logging: true,
  extra: {
    ssl: true,
  },
  cli: {
    migrationsDir: 'src/migration/',
  },
};
